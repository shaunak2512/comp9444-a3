import gym
import tensorflow as tf
import numpy as np
import random

# General Parameters
# -- DO NOT MODIFY --
ENV_NAME = 'CartPole-v0'
EPISODE = 200000  # Episode limitation
STEP = 200  # Step limitation in an episode
TEST = 10  # The number of tests to run every TEST_FREQUENCY episodes
TEST_FREQUENCY = 100  # Num episodes to run before visualizing test accuracy

# TODO: HyperParameters
GAMMA = 0.80  # discount factor
INITIAL_EPSILON = 1  # starting value of epsilon
FINAL_EPSILON = 0.01  # final value of epsilon
EPSILON_DECAY_STEPS = 500  # decay period
LEARNING_RATE = 0.001
BATCH_SIZE = 50
# Create environment
# -- DO NOT MODIFY --
env = gym.make(ENV_NAME)
epsilon = INITIAL_EPSILON
STATE_DIM = env.observation_space.shape[0]
ACTION_DIM = env.action_space.n

# Placeholders
# -- DO NOT MODIFY --
state_in = tf.placeholder("float", [None, STATE_DIM])
action_in = tf.placeholder("float", [None, ACTION_DIM])
target_in = tf.placeholder("float", [None])

# TODO: Define Network Graph
w = tf.Variable(tf.random_uniform(shape=[STATE_DIM, ACTION_DIM]))
l1 = tf.layers.dense(state_in, STATE_DIM,
                     activation=tf.nn.relu, use_bias=False)
l2 = tf.layers.dense(l1, 24, activation=tf.nn.relu, use_bias=False)
l3 = tf.layers.dense(l2, 12, activation=tf.nn.relu, use_bias=False)
res = tf.layers.dense(l3, ACTION_DIM, use_bias=False)
# TODO: Network outputs
#q_values = tf.matmul(state_in,w)
q_values = res
q_action = tf.argmax(q_values, 1)

# TODO: Loss/Optimizer Definition

#print("q_values shape ",q_values.shape)
sub = target_in - q_values
square = tf.square(sub)
loss = tf.reduce_sum(square)
optimizer = tf.train.GradientDescentOptimizer(LEARNING_RATE).minimize(loss)

# Start session - Tensorflow housekeeping
session = tf.InteractiveSession()
session.run(tf.global_variables_initializer())


# -- DO NOT MODIFY ---
def explore(state, epsilon):
    """
    Exploration function: given a state and an epsilon value,
    and assuming the network has already been defined, decide which action to
    take using e-greedy exploration based on the current q-value estimates.
    """
    Q_estimates = q_values.eval(feed_dict={
        state_in: [state]
    })
    if random.random() <= epsilon:
        action = random.randint(0, ACTION_DIM - 1)
    else:
        action = np.argmax(Q_estimates)
    one_hot_action = np.zeros(ACTION_DIM)
    one_hot_action[action] = 1
    return one_hot_action


# Main learning loop
for episode in range(EPISODE):

    # initialize task
    state = env.reset()

    # Update epsilon once per episode
    epsilon -= epsilon / EPSILON_DECAY_STEPS

    target = 0
    experiences = []
    batch = []
    # Move through env according to e-greedy policy
    for step in range(STEP):
        action = explore(state, epsilon)
        next_state, reward, done, _ = env.step(np.argmax(action))
        # Creating a list of experiences
        experiences.append((state, action, reward, next_state, done))
        # Create batches
        if(len(experiences) > BATCH_SIZE):
            batch = random.sample(experiences, BATCH_SIZE)

        for state,action,reward,next_state,done in batch:
        	next_state_q_values_curr =  q_values.eval(feed_dict={state_in: [next_state]})



        nextstate_q_values_batch = [q_values.eval(
            feed_dict={state_in: [next_state]}) for _, _, _, next_state, _ in batch]

        nextstate_q_values = q_values.eval(feed_dict={state_in: [next_state]})

        # TODO: Calculate the target q-value.
        # hint1: Bellman
        # hint2: consider if the episode has terminated
        max_Q = np.max(nextstate_q_values)
        target = target + LEARNING_RATE * \
            (reward + GAMMA * max_Q - target) if not done else reward



        target_batch = [reward + GAMMA * np.max(nextstate_q_values_curr) if not done else reward for (
            _, _, reward, _, _), nextstate_q_values_curr in zip(batch, nextstate_q_values_batch)]

        # Do one training step
        # if len(experiences) > BATCH_SIZE:
        # session.run([optimizer], feed_dict={
        #     target_in: [target],
        #     action_in: [action],
        #     state_in: [state]
        # })

       #print([state for state, _, _, _, _ in batch])
        if len(experiences) > BATCH_SIZE:
            session.run([optimizer], feed_dict={
                target_in: target_batch,
                action_in: [action for _, action, _, _, _ in batch],
                state_in: [state for state, _, _, _, _ in batch]
            })

        # Update
        state = next_state
        if done:
            break

    # Test and view sample runs - can disable render to save time
    # -- DO NOT MODIFY --
    if (episode % TEST_FREQUENCY == 0 and episode != 0):
        total_reward = 0
        for i in range(TEST):
            state = env.reset()
            for j in range(STEP):
                env.render()
                action = np.argmax(q_values.eval(feed_dict={
                    state_in: [state]
                }))
                state, reward, done, _ = env.step(action)
                total_reward += reward
                if done:
                    break
        ave_reward = total_reward / TEST
        print('episode:', episode, 'epsilon:', epsilon, 'Evaluation '
                                                        'Average Reward:', ave_reward)

env.close()
