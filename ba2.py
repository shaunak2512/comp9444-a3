import gym
import tensorflow as tf
import numpy as np
import random

# General Parameters
# -- DO NOT MODIFY --
ENV_NAME = 'CartPole-v0'
EPISODE = 200000  # Episode limitation
STEP = 200  # Step limitation in an episode
TEST = 10  # The number of tests to run every TEST_FREQUENCY episodes
TEST_FREQUENCY = 1  # Num episodes to run before visualizing test accuracy

# TODO: HyperParameters
GAMMA =  0.8 # discount factor
INITIAL_EPSILON =  0.9 # starting value of epsilon
FINAL_EPSILON =  0.01 # final value of epsilon
EPSILON_DECAY_STEPS =  90 # decay period
LEARNING_RATE = 0.001
BATCH_SIZE = 5
# Create environment
# -- DO NOT MODIFY --
env = gym.make(ENV_NAME)
epsilon = INITIAL_EPSILON
STATE_DIM = env.observation_space.shape[0]
ACTION_DIM = env.action_space.n

# Placeholders
# -- DO NOT MODIFY --
state_in = tf.placeholder("float", [None, STATE_DIM])
action_in = tf.placeholder("float", [None, ACTION_DIM])
target_in = tf.placeholder("float", [None])

# TODO: Define Network Graph
w = tf.Variable(tf.random_uniform(shape=[STATE_DIM,ACTION_DIM]))
#First layer
l1 = tf.layers.dense(state_in,STATE_DIM,activation=tf.nn.tanh,use_bias=False)
l2 = tf.layers.dense(l1,24,activation=tf.nn.tanh,use_bias=False)
l3 = tf.layers.dense(l2,12,activation=tf.nn.tanh,use_bias=False)
res = tf.layers.dense(l3,ACTION_DIM,use_bias=False)
# TODO: Network outputs
q_values = res#tf.multiply(res,action_in)
print_action = tf.Print
mul_res = tf.multiply(q_values,action_in)
q_action = tf.reduce_sum(mul_res)
# TODO: Loss/Optimizer Definition
loss = tf.reduce_sum(tf.square(target_in-q_action))
optimizer = tf.train.AdamOptimizer(LEARNING_RATE).minimize(loss)

# Start session - Tensorflow housekeeping
session = tf.InteractiveSession()
session.run(tf.global_variables_initializer())

latest_reward = 0
max_reward=0
experiences = []


# -- DO NOT MODIFY ---
def explore(state, epsilon):
    """
    Exploration function: given a state and an epsilon value,
    and assuming the network has already been defined, decide which action to
    take using e-greedy exploration based on the current q-value estimates.
    """
    Q_estimates = q_values.eval(feed_dict={
        state_in: [state]
    })
    if random.random() <= epsilon:
        action = random.randint(0, ACTION_DIM - 1)
    else:
        action = np.argmax(Q_estimates)
    one_hot_action = np.zeros(ACTION_DIM)
    one_hot_action[action] = 1
    return one_hot_action

# Main learning loop
for episode in range(EPISODE):

    # initialize task
    state = env.reset()

    # Update epsilon once per episode
    epsilon -= epsilon / EPSILON_DECAY_STEPS
    target = 0
    minibatch = []

    if (max_reward<15):
            epsilon=1
            
    if(latest_reward==200):
        LEARNING_RATE=0
    

    # Move through env according to e-greedy policy
    for step in range(STEP):
        action = explore(state, epsilon)
        next_state, reward, done, _ = env.step(np.argmax(action))
        if done:
            print(step, len(experiences))

        if (step==199):
            reward=1000000
        #nextstate_q_values = q_values.eval(feed_dict={
        #    state_in: [next_state]
        #})

        # Creating a list of experiences
        if (latest_reward > 20):
            experiences.append((state, action, reward, next_state, done))

        
        
        minibatch.append((state, action, reward, next_state, done))
        #print(LEARNING_RATE)
        #if( latest_reward>100):
        #    BATCH_SIZE= round(BATCH_SIZE - (latest_reward-100)/10)
        if(len(minibatch) == BATCH_SIZE):
            batch = np.array(random.sample(minibatch, len(minibatch)))
            #Calculate next state q values for all states in batch
            nextstate_q_values_batch =[q_values.eval(
                feed_dict={state_in: [next_state]}) for _, _, _, next_state, _ in batch] 
            #Calculate target q value for all experiences in batch
            target_batch = np.array([reward + (GAMMA * np.max(nextstate_q_values_curr)) if not done else reward for (
                _, _, reward, _, _), nextstate_q_values_curr in zip(batch, nextstate_q_values_batch)])
            #Get all action in batch in an np array
            action_batch = np.array([action for _, action, _, _, _ in batch])
            #Get all the states in batch in an np array
            state_batch = np.array([state for state, _, _, _, _ in batch])
            
            session.run([optimizer], feed_dict={
                target_in: target_batch,
                action_in: action_batch, 
                state_in: state_batch
            })
            minibatch=[]
        # Create batches

        if(len(experiences) > 40):
            batch = np.array(random.sample(experiences, 40))
            #Calculate next state q values for all states in batch
            nextstate_q_values_batch =[q_values.eval(
                feed_dict={state_in: [next_state]}) for _, _, _, next_state, _ in batch] 
            #Calculate target q value for all experiences in batch
            target_batch = np.array([reward + (GAMMA * np.max(nextstate_q_values_curr)) if not done else reward for (
                _, _, reward, _, _), nextstate_q_values_curr in zip(batch, nextstate_q_values_batch)])
            #Get all action in batch in an np array
            action_batch = np.array([action for _, action, _, _, _ in batch])
            #Get all the states in batch in an np array
            state_batch = np.array([state for state, _, _, _, _ in batch])
            
            session.run([optimizer], feed_dict={
                target_in: target_batch,
                action_in: action_batch, 
                state_in: state_batch
            })
        #     #experiences = []
        
        # TODO: Calculate the target q-value.
        # hint1: Bellman
        # hint2: consider if the episode has terminated
        #max_Q = np.max(nextstate_q_values)
       
        #target = target + LEARNING_RATE*(reward + GAMMA*max_Q - target) if not done else reward
       
        # # Do one training step
        # if(len(experiences) > BATCH_SIZE):
        #     session.run([optimizer], feed_dict={
        #         target_in: [target],
        #         action_in: [action],
        #         state_in: [state]
        #     })

        # Update
        state = next_state
        if done:
            break

    # Test and view sample runs - can disable render to save time
    # -- DO NOT MODIFY --
    if (episode % TEST_FREQUENCY == 0 and episode != 0):
        total_reward = 0
        for i in range(TEST):
            state = env.reset()
            for j in range(STEP):
                #env.render()
                action = np.argmax(q_values.eval(feed_dict={
                    state_in: [state]
                }))
                state, reward, done, _ = env.step(action)
                total_reward += reward
                if done:
                    break
        ave_reward = total_reward / TEST
        print('episode:', episode, 'epsilon:', epsilon, 'Evaluation '
                    'batch size:', BATCH_SIZE, 'Average Reward:', ave_reward)
        latest_reward=ave_reward
        if ave_reward > max_reward:
            max_reward=ave_reward
        #if(ave_reward==200):
            #LEARNING_RATE=0

env.close()
